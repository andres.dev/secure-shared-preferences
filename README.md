# Secure Shared Preferences
[![API](https://img.shields.io/badge/API-18%2B-brightgreen.svg?style=flat)](https://android-arsenal.com/api?level=18)

An Android Shared Preferences Implementation that encrypts all the stored data.

This library uses asymmetric encryption based on RSA/ECB/PKCS1PADDING. It also uses the Android KeyStore to save the secret key. For this reason, the minimun API version to use this library is 18 (Android 4.3 Jelly Bean). 

You can save and load Double type values with this library. (the native SharedPreferences can't!)

Note: If you change your password, PIN, or unlock pattern in your phone, you won't be able to access your encrypted data anymore. Therefore, use this library only to save data that you can recover again, such as server tokens, secret api keys, session values, user data, etc.

### Import
```Gradle
//app module build.gradle
dependencies {
    ...
    implementation 'pe.hexagonsys:securesharedpreferences:1.0-beta2'
    ...
}
```
### How to use

```kotlin
package com.example.demo

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import com.example.demo


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val initialValue = "ultra secret phrase"
        val prefs = getSharedPreferences("com.example.demo", Context.MODE_PRIVATE)
        val securePrefs = SecureSharedPreferences(this, prefs, "awesome alias")
        securePrefs.Editor().putString("stringTest", initialValue).commit()
        val finalValue = securePrefs.getString("stringTest", null)
        if(initialValue == finalValue) tvwMessage.text = finalValue
    }

}

```

### Sample
Browse the sample code [here](https://gitlab.com/andres.dev/secure-shared-preferences/tree/master/app).
### Unit tests
You can see all unit tests [here](https://gitlab.com/andres.dev/secure-shared-preferences/blob/master/securesharedpreferences/src/androidTest/java/pe/hexagonsys/securesharedpreferences/ExampleInstrumentedTest.kt).

### Developed By
Andrés Escobar - [andresescobarvilla@gmail.com](mailto:andresescobarvilla@gmail.com) / [andres_escobar@hexagonsys.pe](mailto:andres_escobar@hexagonsys.pe)

You can find me in [Linkedin](https://www.linkedin.com/in/andres-escobar-villa).

