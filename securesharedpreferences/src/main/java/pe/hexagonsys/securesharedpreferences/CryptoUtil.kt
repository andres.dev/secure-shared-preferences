package pe.hexagonsys.securesharedpreferences

import android.content.Context
import android.security.KeyPairGeneratorSpec
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Base64
import java.math.BigInteger
import java.nio.charset.Charset
import java.security.KeyPairGenerator
import java.security.KeyStore
import java.security.PrivateKey
import java.security.spec.AlgorithmParameterSpec
import java.util.*
import javax.crypto.Cipher
import javax.security.auth.x500.X500Principal

class CryptoUtil {

    companion object {

        private const val provider = "AndroidKeyStore"
        private const val transformation = "RSA/ECB/PKCS1PADDING"
        private const val charset = "UTF-8"
        private const val algorithm = "RSA"
        private const val commonName = "CN"

        fun checkIfKeyPairExists(keyAlias: String): Boolean {
            val keyStore = loadKeyStore()
            val entry = keyStore.getEntry(keyAlias,null)
            return entry != null
        }

        fun createKeyPair(context: Context, keyAlias: String) {

            val start = GregorianCalendar.getInstance()
            val end = GregorianCalendar.getInstance().apply { add(1, Calendar.YEAR) }

            val spec: AlgorithmParameterSpec
            val kpg: KeyPairGenerator
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                spec = KeyGenParameterSpec.Builder(
                    keyAlias,
                    KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
                )
                    .setCertificateSubject(X500Principal("$commonName=$keyAlias"))
                    .setCertificateSerialNumber(BigInteger.TEN)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1)
                    .setCertificateNotBefore(start.time)
                    .setCertificateNotAfter(end.time)
                    .setDigests(
                        KeyProperties.DIGEST_SHA256,
                        KeyProperties.DIGEST_SHA512
                    )
                    .build()
                kpg = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, provider)
            } else {
                spec = KeyPairGeneratorSpec.Builder(context)
                    .setAlias(keyAlias)
                    .setSubject(X500Principal("$commonName=$keyAlias"))
                    .setSerialNumber(BigInteger.TEN)
                    .setStartDate(start.time)
                    .setEndDate(end.time)
                    .build()
                kpg = KeyPairGenerator.getInstance(algorithm, provider)
            }
            kpg.initialize(spec)
            kpg.generateKeyPair()
        }

        fun removeKeyPair(keyAlias: String){
            loadKeyStore().deleteEntry(keyAlias)
        }

        fun encrypt(phrase: String, keyAlias: String): String {
            val keyStore = loadKeyStore()
            val publicKey = keyStore.getCertificate(keyAlias)
            val cipher = Cipher.getInstance(transformation)
            cipher.init(Cipher.ENCRYPT_MODE, publicKey)
            val encrypted = cipher.doFinal(phrase.toByteArray(Charset.forName(charset)))
            return Base64.encodeToString(encrypted, Base64.DEFAULT)
        }

        fun decrypt(encryptedPhrase: String, keyAlias: String): String {
            val keyStore = loadKeyStore()
            val privateKey = keyStore.getKey(keyAlias, null) as PrivateKey?
            val decipher = Cipher.getInstance(transformation)
            decipher.init(Cipher.DECRYPT_MODE, privateKey)
            val encryptedBytes = Base64.decode(encryptedPhrase, Base64.DEFAULT)
            val decrypted = decipher.doFinal(encryptedBytes)
            return String(decrypted, 0, decrypted.size, Charset.forName(charset))
        }

        private fun loadKeyStore(): KeyStore {
            val keyStore = KeyStore.getInstance(provider)
            keyStore.load(null)
            return keyStore
        }

    }

}