package pe.hexagonsys.securesharedpreferences

import android.content.Context
import android.content.SharedPreferences


class SecureSharedPreferences(context: Context, val delegate: SharedPreferences, val keyAlias: String) :
    SharedPreferences {

    init {
        if(!CryptoUtil.checkIfKeyPairExists(keyAlias)) {
            CryptoUtil.createKeyPair(context, keyAlias)
        }
    }

    override fun contains(key: String?): Boolean {
        return delegate.contains(key)
    }

    override fun edit(): SharedPreferences.Editor {
        return delegate.edit()
    }

    override fun getAll(): MutableMap<String, *> {
        val all = delegate.all
        val unencryptedMap = HashMap<String, String>(all.size)
        all.keys.forEach { key ->
            all[key]?.let {
                unencryptedMap[key] = CryptoUtil.decrypt(it.toString(), keyAlias)
            }
        }
        return unencryptedMap
    }

    override fun getBoolean(key: String?, defaultValue: Boolean): Boolean {
        return delegate.getString(key, null)?.let {
            CryptoUtil.decrypt(it, keyAlias).toBoolean()
        } ?: defaultValue

    }

    fun getDouble(key: String, defValue: Double?): Double? {
        return defValue?.let {
            java.lang.Double.longBitsToDouble(

                    getLong(
                        key,
                        java.lang.Double.doubleToRawLongBits(it)
                    )
            )
        }
    }

    override fun getFloat(key: String?, defaultValue: Float): Float {
        return delegate.getString(key, null)?.let {
            CryptoUtil.decrypt(it, keyAlias).toFloat()
        } ?: defaultValue
    }

    override fun getInt(key: String?, defaultValue: Int): Int {
        return delegate.getString(key, null)?.let {
            CryptoUtil.decrypt(it, keyAlias).toInt()
        } ?: defaultValue
    }

    override fun getLong(key: String?, defaultValue: Long): Long {
        return delegate.getString(key, null)?.let {
            CryptoUtil.decrypt(it, keyAlias).toLong()
        } ?: defaultValue
    }


    override fun getString(key: String?, defaultValue: String?): String? {
        return delegate.getString(key, null)?.let {
            CryptoUtil.decrypt(it, keyAlias)
        } ?: defaultValue
    }

    override fun getStringSet(key: String?, defaultValue: MutableSet<String>?): MutableSet<String>? {
        return delegate.getStringSet(key, null)?.let {
            val decryptedSet = HashSet<String>()
            it.forEach { encryptedString ->
                decryptedSet.add(CryptoUtil.decrypt(encryptedString, keyAlias))
            }
            decryptedSet
        } ?: defaultValue
    }

    override fun registerOnSharedPreferenceChangeListener(listener: SharedPreferences.OnSharedPreferenceChangeListener?) {
        delegate.registerOnSharedPreferenceChangeListener(listener)
    }

    override fun unregisterOnSharedPreferenceChangeListener(key: SharedPreferences.OnSharedPreferenceChangeListener?) {
        delegate.unregisterOnSharedPreferenceChangeListener(key)
    }

    inner class Editor : SharedPreferences.Editor {

        private var delegate: SharedPreferences.Editor = this@SecureSharedPreferences.delegate.edit()

        override fun putBoolean(key: String, value: Boolean): SharedPreferences.Editor {
            delegate.putString(key, CryptoUtil.encrypt(value.toString(), keyAlias))
            return this
        }

        fun putDouble(key: String, value: Double): Editor {
            putLong(key, java.lang.Double.doubleToRawLongBits(value))
            return this
        }

        override fun putFloat(key: String, value: Float): SharedPreferences.Editor {
            delegate.putString(key, CryptoUtil.encrypt(value.toString(), keyAlias))
            return this
        }

        override fun putInt(key: String, value: Int): SharedPreferences.Editor {
            delegate.putString(key, CryptoUtil.encrypt(value.toString(), keyAlias))
            return this
        }

        override fun putLong(key: String, value: Long): SharedPreferences.Editor {
            delegate.putString(key, CryptoUtil.encrypt(value.toString(), keyAlias))
            return this
        }

        override fun putString(key: String, value: String?): SharedPreferences.Editor {
            delegate.putString(key, value?.let { CryptoUtil.encrypt(it, keyAlias) })
            return this
        }

        override fun putStringSet(key: String, value: Set<String>?): SharedPreferences.Editor {
            value?.let {
                synchronized(this){
                    val encryptedSet = HashSet<String>()
                    it.forEach { string ->
                        encryptedSet.add(CryptoUtil.encrypt(string, keyAlias))
                    }
                    delegate.putStringSet(key, encryptedSet)
                }
            }
            return this
        }

        override fun remove(key: String): SharedPreferences.Editor {
            delegate.remove(key)
            return this
        }

        override fun clear(): SharedPreferences.Editor {
            delegate.clear()
            return this
        }

        override fun commit(): Boolean {
            return delegate.commit()
        }

        override fun apply() {
            delegate.apply()
        }
    }


}
