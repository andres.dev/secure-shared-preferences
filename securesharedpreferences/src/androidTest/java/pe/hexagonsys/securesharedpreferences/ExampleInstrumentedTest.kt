package pe.hexagonsys.securesharedpreferences

import android.content.Context
import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*


/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    lateinit var instrumentationContext: Context

    @Before
    fun setup() {
        instrumentationContext = InstrumentationRegistry.getInstrumentation().context
    }

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()

        assertEquals("pe.hexagonsys.securesharedpreferences.test", appContext.packageName)
    }

    @Test
    fun checkIfKeyPairCanBeCreated(){
        CryptoUtil.createKeyPair(instrumentationContext, "awesome key alias")
        assertTrue(CryptoUtil.checkIfKeyPairExists("awesome key alias"))
    }

    @Test
    fun checkIfKeyPairCanBeDeleted(){
        CryptoUtil.removeKeyPair("awesome key alias")
        assertTrue(!CryptoUtil.checkIfKeyPairExists("awesome key alias"))
    }

    @Test
    fun checkIfSharedPreferencesCanSaveAnInteger(){
        val initialValue = 4
        val prefs = instrumentationContext.getSharedPreferences("pe.hexagonsys.securesharedpreferences", Context.MODE_PRIVATE)
        val securePrefs = SecureSharedPreferences(instrumentationContext, prefs, "awesome key alias")
        securePrefs.Editor().putInt("intTest", initialValue).commit()
        val finalValue = securePrefs.getInt("intTest", 0)
        assertEquals(initialValue, finalValue)
    }

    @Test
    fun checkIfSharedPreferencesCanSaveAString(){
        val initialValue = "ultra secret phrase"
        val prefs = instrumentationContext.getSharedPreferences("pe.hexagonsys.securesharedpreferences", Context.MODE_PRIVATE)
        val securePrefs = SecureSharedPreferences(instrumentationContext, prefs, "awesome key alias")
        securePrefs.Editor().putString("stringTest", initialValue).commit()
        val finalValue = securePrefs.getString("stringTest", null)
        assertEquals(initialValue, finalValue)
    }

    @Test
    fun checkIfSharedPreferencesCanSaveAnFloat(){
        val initialValue = 4f
        val prefs = instrumentationContext.getSharedPreferences("pe.hexagonsys.securesharedpreferences", Context.MODE_PRIVATE)
        val securePrefs = SecureSharedPreferences(instrumentationContext, prefs, "awesome key alias")
        securePrefs.Editor().putFloat("floatTest", initialValue).commit()
        val finalValue = securePrefs.getFloat("floatTest", 0f)
        assertEquals(initialValue, finalValue)
    }

    @Test
    fun checkIfSharedPreferencesCanSaveAnLong(){
        val initialValue = 4L
        val prefs = instrumentationContext.getSharedPreferences("pe.hexagonsys.securesharedpreferences", Context.MODE_PRIVATE)
        val securePrefs = SecureSharedPreferences(instrumentationContext, prefs, "awesome key alias")
        securePrefs.Editor().putLong("longTest", initialValue).commit()
        val finalValue = securePrefs.getLong("longTest", 0L)
        assertEquals(initialValue, finalValue)
    }

    @Test
    fun checkIfSharedPreferencesCanSaveAnDouble(){
        val initialValue = 4.123456789
        val prefs = instrumentationContext.getSharedPreferences("pe.hexagonsys.securesharedpreferences", Context.MODE_PRIVATE)
        val securePrefs = SecureSharedPreferences(instrumentationContext, prefs, "awesome key alias")
        securePrefs.Editor().putDouble("doubleTest", initialValue).commit()
        val finalValue = securePrefs.getDouble("doubleTest", 0.0)
        assertEquals(initialValue, finalValue)
    }

    @Test
    fun checkIfSharedPreferencesCanSaveABoolean(){
        val initialValue = true
        val prefs = instrumentationContext.getSharedPreferences("pe.hexagonsys.securesharedpreferences", Context.MODE_PRIVATE)
        val securePrefs = SecureSharedPreferences(instrumentationContext, prefs, "awesome key alias")
        securePrefs.Editor().putBoolean("booleanTest", initialValue).commit()
        val finalValue = securePrefs.getBoolean("booleanTest", false)
        assertEquals(initialValue, finalValue)
    }

    @Test
    fun checkIfSharedPreferencesCanSaveAStringSet(){
        val initialValue = HashSet<String>()
        initialValue.add("this")
        initialValue.add("is")
        initialValue.add("a")
        initialValue.add("secret")
        initialValue.add("phrase")
        val prefs = instrumentationContext.getSharedPreferences("pe.hexagonsys.securesharedpreferences", Context.MODE_PRIVATE)
        val securePrefs = SecureSharedPreferences(instrumentationContext, prefs, "awesome key alias")
        securePrefs.Editor().putStringSet("stringSetTest", initialValue).commit()
        val finalValue = securePrefs.getStringSet("stringSetTest", null)
        assertEquals(initialValue, finalValue)
    }

    //null values

    @Test
    fun checkIfSharedPreferencesCanGetANullInteger(){
        val prefs = instrumentationContext.getSharedPreferences("pe.hexagonsys.securesharedpreferences", Context.MODE_PRIVATE)
        val securePrefs = SecureSharedPreferences(instrumentationContext, prefs, "awesome key alias")
        val nullValue = securePrefs.getInt("nullValue", 0)
        assertEquals(0, nullValue)
    }




}
