package pe.hexagonsys.securesharedpreferences.demo

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import pe.hexagonsys.securesharedpreferences.SecureSharedPreferences


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val initialValue = "ultra secret phrase"
        val prefs = getSharedPreferences("pe.hexagonsys.securesharedpreferences.demo", Context.MODE_PRIVATE)
        val securePrefs = SecureSharedPreferences(this, prefs, "lol")
        securePrefs.Editor().putString("stringTest", initialValue).commit()
        val finalValue = securePrefs.getString("stringTest", null)
        tvwMessage.text =
            if(initialValue == finalValue)
                finalValue
            else
                "phrase couldn't be encrypted correctly"
    }
}
